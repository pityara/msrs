use async_trait::async_trait;
use std::sync::RwLock;

use crate::errors::msrs::MsRsError;

use super::transport::{MsMessage, Transport};

pub struct MockTransport {
    event: RwLock<String>,
    data: RwLock<String>,
}

impl MockTransport {
    pub fn _new() -> Self {
        MockTransport {
            event: RwLock::new(String::new()),
            data: RwLock::new(String::new()),
        }
    }
}

#[async_trait]
impl Transport for MockTransport {
    async fn send(&self, event: &str, data: MsMessage) -> Result<(), MsRsError> {
        let mut event_write = self.event.write().unwrap();
        *event_write = event.to_string();
        let mut data_write = self.data.write().unwrap();
        *data_write = data.1.to_string();

        Ok(())
    }

    async fn listen_response(
        &self,
        _event: &str,
        predicate: Box<dyn Fn(MsMessage) -> bool + Send + Sync>,
    ) -> Result<MsMessage, MsRsError> {
        let data = self.data.read().unwrap().clone();
        if predicate(MsMessage(None, data)) {
            Ok(MsMessage(None, self.data.read().unwrap().clone()))
        } else {
            Ok(MsMessage(None, "wrong_MsMessage".to_string()))
        }
    }

    async fn consume(&self, event: &str) -> Result<MsMessage, MsRsError> {
        assert_eq!(event, "test_event");
        Ok(MsMessage(None, "consumed_MsMessage".to_string()))
    }
}

#[tokio::test]
async fn test_send() {
    let transport = MockTransport::_new();
    transport
        .send("test_event", MsMessage(None, "test_data".to_string()))
        .await
        .unwrap();
}

#[tokio::test]
async fn test_listen() {
    let transport = MockTransport::_new();
    let predicate: Box<dyn Fn(MsMessage) -> bool + Send + Sync> =
        Box::new(|msg| msg.1 == "test_MsMessage");
    let _ = transport
        .send("test_event", MsMessage(None, "test_MsMessage".to_string()))
        .await;
    assert_eq!(
        transport
            .listen_response("test_event", predicate)
            .await
            .unwrap()
            .1,
        "test_MsMessage"
    );
}

#[tokio::test]
async fn test_consume() {
    let transport = MockTransport::_new();
    assert_eq!(
        transport.consume("test_event").await.unwrap().1,
        "consumed_MsMessage"
    );
}
