use std::collections::HashMap;

use async_trait::async_trait;

use crate::errors::msrs::MsRsError;
pub struct MsMessage(pub Option<HashMap<String, String>>, pub String);

#[async_trait]
pub trait Transport: Sync + Send {
    async fn send(self: &Self, event: &str, data: MsMessage) -> Result<(), MsRsError>;
    async fn listen_response(
        &self,
        event: &str,
        predicate: Box<dyn Fn(MsMessage) -> bool + Send + Sync>,
    ) -> Result<MsMessage, MsRsError>;
    async fn consume(&self, event: &str) -> Result<MsMessage, MsRsError>;
}
