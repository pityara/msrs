use serde::Serialize;
use thiserror::Error;

#[derive(Debug, Error, Serialize)]
pub enum MsRsError {
    #[error("Got error response for sync request: {0}")]
    ResoponseError(String),

    #[error("Got error while subscribing: {0}")]
    SubscribeError(String),

    #[error("Got error while receiving the message: {0}")]
    ReceiveError(String),

    #[error("Got error while commiting the message: {0}")]
    CommitError(String),

    #[error("Got error while trying to send a message: {0}")]
    SendError(String),

    #[error("{0}")]
    RpcError(String),

    #[error("An internal error occurs: {0}")]
    InternalError(String)
}
