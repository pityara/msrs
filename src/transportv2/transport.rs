use async_trait::async_trait;
use serde::{de::DeserializeOwned, Serialize};

use crate::errors::msrs::MsRsError;

pub type ReqResp<Request, Response> = (
    Request,
    tokio::sync::oneshot::Sender<Result<Option<Response>, MsRsError>>,
);
pub type RequestResponseChannel<Request, Response> =
    tokio::sync::mpsc::Sender<ReqResp<Request, Response>>;

#[async_trait]
pub trait TransportV2 {
    async fn listen_responses(&self, method: String);
    async fn send<Request, Response>(
        &self,
        method: String,
        request: Request,
    ) -> Result<Response, MsRsError>
    where
        Request: Serialize + Send,
        Response: DeserializeOwned + Send;

    async fn send_event<Event>(&self, event_name: String, event: Event) -> Result<(), MsRsError>
    where
        Event: Serialize + Send;
    async fn listen<Request, Response>(
        &self,
        method: String,
        channel: RequestResponseChannel<Request, Response>,
    ) -> Result<(), MsRsError>
    where
        Request: Serialize + DeserializeOwned + Send,
        Response: Serialize + DeserializeOwned + Send;
}
